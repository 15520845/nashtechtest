package com.nashtech.exercises.icecream.dao;

import javax.persistence.*;

@Entity
@Table(name = "online_order")
public class OnlineOrder {
    @Id
    @Column(name = "order_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String name;
    private String email;
    private String contact;
    private String address;
    private long bookCost;
    private String payingOption;
    private boolean orderDate;
    private boolean status;

    public OnlineOrder(String name, String email, String contact, String address, long bookCost, String payingOption, boolean orderDate, boolean status) {
        this.name = name;
        this.email = email;
        this.contact = contact;
        this.address = address;
        this.bookCost = bookCost;
        this.payingOption = payingOption;
        this.orderDate = orderDate;
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public long getBookCost() {
        return bookCost;
    }

    public void setBookCost(long bookCost) {
        this.bookCost = bookCost;
    }

    public String getPayingOption() {
        return payingOption;
    }

    public void setPayingOption(String payingOption) {
        this.payingOption = payingOption;
    }

    public boolean isOrderDate() {
        return orderDate;
    }

    public void setOrderDate(boolean orderDate) {
        this.orderDate = orderDate;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
}
