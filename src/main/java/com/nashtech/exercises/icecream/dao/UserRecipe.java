package com.nashtech.exercises.icecream.dao;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "user_recipe")
public class UserRecipe {
    @Id
    @Column(name = "user_recipe_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String name;
    private String image;
    private String description;
    private String details;
    private boolean prizeStatus;
    private boolean enableStatus;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "customer_id", referencedColumnName = "customer_id")
    private Customer customer;

    public UserRecipe() {

    }

    public UserRecipe(String name, String image, String description, String details, boolean prizeStatus, boolean enableStatus) {
        this.name =name;
        this.image = image;
        this.description = description;
        this.details = details;
        this.prizeStatus = prizeStatus;
        this.enableStatus = enableStatus;
    }

    public UserRecipe(Customer customer, String name, String image, String description, String details, boolean prizeStatus, boolean enableStatus) {
        this.customer = customer;
        this.name =name;
        this.image = image;
        this.description = description;
        this.details = details;
        this.prizeStatus = prizeStatus;
        this.enableStatus = enableStatus;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public boolean isPrizeStatus() {
        return prizeStatus;
    }

    public void setPrizeStatus(boolean prizeStatus) {
        this.prizeStatus = prizeStatus;
    }

    public boolean isEnableStatus() {
        return enableStatus;
    }

    public void setEnableStatus(boolean enableStatus) {
        this.enableStatus = enableStatus;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }
}
