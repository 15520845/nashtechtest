package com.nashtech.exercises.icecream.dao;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "reference")
public class Reference {
    @Id
    @Column(name = "reference_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private BigDecimal monthlyFee;
    private BigDecimal yearlyFee;
    private BigDecimal bookCost;

    public Reference() {
    }

    public Reference(BigDecimal monthlyFee, BigDecimal yearlyFee, BigDecimal bookCost) {
        this.monthlyFee = monthlyFee;
        this.yearlyFee = yearlyFee;
        this.bookCost = bookCost;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public BigDecimal getMonthlyFee() {
        return monthlyFee;
    }

    public void setMonthlyFee(BigDecimal monthlyFee) {
        this.monthlyFee = monthlyFee;
    }

    public BigDecimal getYearlyFee() {
        return yearlyFee;
    }

    public void setYearlyFee(BigDecimal yearlyFee) {
        this.yearlyFee = yearlyFee;
    }

    public BigDecimal getBookCost() {
        return bookCost;
    }

    public void setBookCost(BigDecimal bookCost) {
        this.bookCost = bookCost;
    }
}
