package com.nashtech.exercises.icecream.dao;

import java.time.LocalDate;
import java.util.Optional;

public class CustomerDto {
    private int id;
    private Optional<String> username;
    private Optional<String> password;
    private Optional<String> fullName;
    private Optional<String> address;
    private Optional<String> phoneNumber;
    private Optional<String> email;
    private Optional<String> gender;
    private Optional<LocalDate> birthday;
    private Optional<String> avatar;
    private Optional<LocalDate> expiredDate;
    private Optional<Boolean> enableStatus;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Optional<String> getUsername() {
        return username;
    }

    public void setUsername(Optional<String> username) {
        this.username = username;
    }

    public Optional<String> getPassword() {
        return password;
    }

    public void setPassword(Optional<String> password) {
        this.password = password;
    }

    public Optional<String> getFullName() {
        return fullName;
    }

    public void setFullName(Optional<String> fullName) {
        this.fullName = fullName;
    }

    public Optional<String> getAddress() {
        return address;
    }

    public void setAddress(Optional<String> address) {
        this.address = address;
    }

    public Optional<String> getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(Optional<String> phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Optional<String> getEmail() {
        return email;
    }

    public void setEmail(Optional<String> email) {
        this.email = email;
    }

    public Optional<String> getGender() {
        return gender;
    }

    public void setGender(Optional<String> gender) {
        this.gender = gender;
    }

    public Optional<LocalDate> getBirthday() {
        return birthday;
    }

    public void setBirthday(Optional<LocalDate> birthday) {
        this.birthday = birthday;
    }

    public Optional<String> getAvatar() {
        return avatar;
    }

    public void setAvatar(Optional<String> avatar) {
        this.avatar = avatar;
    }

    public Optional<LocalDate> getExpiredDate() {
        return expiredDate;
    }

    public void setExpiredDate(Optional<LocalDate> expiredDate) {
        this.expiredDate = expiredDate;
    }

    public Optional<Boolean> getEnableStatus() {
        return enableStatus;
    }

    public void setEnableStatus(Optional<Boolean> enableStatus) {
        this.enableStatus = enableStatus;
    }
}
