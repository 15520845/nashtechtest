package com.nashtech.exercises.icecream.reponsitory;

import com.nashtech.exercises.icecream.dao.UserRecipe;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRecipeRepository extends JpaRepository<UserRecipe, Integer> {

    @Query(value = "SELECT * FROM user_recipe order by user_recipe_id desc limit 10 ", nativeQuery = true)
    List<UserRecipe> getNewestUserRecipe();
}
