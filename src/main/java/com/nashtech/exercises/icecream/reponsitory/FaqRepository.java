package com.nashtech.exercises.icecream.reponsitory;

import com.nashtech.exercises.icecream.dao.Faq;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FaqRepository extends JpaRepository<Faq, String> {
}
