package com.nashtech.exercises.icecream.reponsitory;

import com.nashtech.exercises.icecream.dao.OnlineOrder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OnlineOrderRepository extends JpaRepository<OnlineOrder, Integer> {
}
