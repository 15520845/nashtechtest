package com.nashtech.exercises.icecream.reponsitory;

import com.nashtech.exercises.icecream.dao.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Integer> {
}
