package com.nashtech.exercises.icecream.reponsitory;

import com.nashtech.exercises.icecream.dao.Recipe;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RecipeRepository extends JpaRepository<Recipe, Integer> {
    @Query(value = "select * from recipe order by view_number desc limit 10", nativeQuery = true)
    List<Recipe> getTopMostViewRecipe();
}