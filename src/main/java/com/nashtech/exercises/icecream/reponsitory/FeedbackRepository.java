package com.nashtech.exercises.icecream.reponsitory;

import com.nashtech.exercises.icecream.dao.Faq;
import com.nashtech.exercises.icecream.dao.Feedback;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FeedbackRepository extends JpaRepository<Feedback, Integer> {
}
