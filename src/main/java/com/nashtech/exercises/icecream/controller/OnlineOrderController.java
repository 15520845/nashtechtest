package com.nashtech.exercises.icecream.controller;

import com.nashtech.exercises.icecream.dao.Faq;
import com.nashtech.exercises.icecream.dao.OnlineOrder;
import com.nashtech.exercises.icecream.service.FaqService;
import com.nashtech.exercises.icecream.service.OnlineOrderService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class OnlineOrderController {
    private OnlineOrderService onlineOrderService;

    public OnlineOrderController(OnlineOrderService onlineOrderService) {
        this.onlineOrderService = onlineOrderService;
    }

    @GetMapping(value = "/onlineOrder")
    public List<OnlineOrder> findALl(){
        return onlineOrderService.findAll();
    }

    @PostMapping(value = "/onlineOrder")
    @ResponseStatus(HttpStatus.CREATED)
    public OnlineOrder create(@RequestBody OnlineOrder onlineOrder) {
        return onlineOrderService.save(onlineOrder);
    }
}
