package com.nashtech.exercises.icecream.controller;

import com.nashtech.exercises.icecream.service.UserRecipeService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class UserRecipeController {
    private UserRecipeService userRecipeService;

    public UserRecipeController(UserRecipeService userRecipeService) {
        this.userRecipeService = userRecipeService;
    }

    @GetMapping(value = "/newestUserRecipe")
    public List<String> getTopNewestUserRecipe(){
        return userRecipeService.getNewestUserRecipe();
    }
}
