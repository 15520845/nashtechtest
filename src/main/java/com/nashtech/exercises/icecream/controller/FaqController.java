package com.nashtech.exercises.icecream.controller;

import com.nashtech.exercises.icecream.dao.Customer;
import com.nashtech.exercises.icecream.dao.Faq;
import com.nashtech.exercises.icecream.service.FaqService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class FaqController {
    private FaqService faqService;

    public FaqController(FaqService faqService) {
        this.faqService = faqService;
    }

    @GetMapping(value = "/faq")
    public List<Faq> findALl(){
        return faqService.findAll();
    }

    @PostMapping(value = "/faq")
    @ResponseStatus(HttpStatus.CREATED)
    public Faq create(@RequestBody Faq faq) {
        return faqService.save(faq);
    }
}
