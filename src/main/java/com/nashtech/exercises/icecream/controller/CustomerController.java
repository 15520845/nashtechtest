package com.nashtech.exercises.icecream.controller;

import com.nashtech.exercises.icecream.dao.Customer;
import com.nashtech.exercises.icecream.dao.CustomerDto;
import com.nashtech.exercises.icecream.service.CustomerService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class CustomerController {
    private CustomerService customerService;

    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @GetMapping(value = "/customer")
    public List<Customer> findAll(){
        return customerService.findAll();
    }

    @PostMapping(value = "/customer")
    @ResponseStatus(HttpStatus.CREATED)
    public Customer create(@RequestBody Customer customer) {
        return customerService.save(customer);
    }

//    @PatchMapping(value = "/customer/{id}")
//    @ResponseStatus(HttpStatus.OK)
//    public Customer update(@PathVariable int id, @RequestBody CustomerDto customerDto){
//        customerDto.setId(id);
//        return customerService.update(customerDto);
//    }
}
