package com.nashtech.exercises.icecream.controller;

import com.nashtech.exercises.icecream.dao.Faq;
import com.nashtech.exercises.icecream.dao.Feedback;
import com.nashtech.exercises.icecream.service.FaqService;
import com.nashtech.exercises.icecream.service.FeedbackService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class FeedbackController {
    private FeedbackService feedbackService;

    public FeedbackController(FeedbackService feedbackService) {
        this.feedbackService = feedbackService;
    }

    @GetMapping(value = "/feedback")
    public List<Feedback> findAll(){
        return feedbackService.findAll();
    }

    @PostMapping(value = "/feedback")
    @ResponseStatus(HttpStatus.CREATED)
    public Feedback create(@RequestBody Feedback feedback) {
        return feedbackService.save(feedback);
    }
}
