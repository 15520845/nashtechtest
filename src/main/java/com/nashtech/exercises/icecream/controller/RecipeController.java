package com.nashtech.exercises.icecream.controller;

import com.nashtech.exercises.icecream.dao.Recipe;
import com.nashtech.exercises.icecream.service.RecipeService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

@RestController
public class RecipeController {
    private RecipeService recipeService;

    public RecipeController(RecipeService recipeService) {
        this.recipeService = recipeService;
    }

    @GetMapping(value = "/recipe")
    public List<Recipe> findAll(){
        return recipeService.findAll();
    }

    @GetMapping(value = "/recipe/{id}")
    public Recipe findAll(@PathVariable int id){
        return recipeService.findById(id);
    }

    @GetMapping(value = "/recipe/mostView")
    public List<Recipe> getTopMostViewRecipe(){
        return recipeService.getTopMostViewRecipe();
    }
}
