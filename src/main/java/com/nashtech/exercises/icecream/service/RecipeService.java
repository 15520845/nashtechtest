package com.nashtech.exercises.icecream.service;

import com.nashtech.exercises.icecream.dao.Recipe;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public interface RecipeService {
    List<Recipe> findAll();
    Recipe findById(int recipeId);
    List<Recipe> getTopMostViewRecipe();
}
