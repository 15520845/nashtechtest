package com.nashtech.exercises.icecream.service;

import com.nashtech.exercises.icecream.dao.Customer;
import com.nashtech.exercises.icecream.dao.CustomerDto;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface CustomerService {
    List<Customer> findAll();
    Customer save(Customer customer);
//    Customer update(CustomerDto customerDto);
}
