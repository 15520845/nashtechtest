package com.nashtech.exercises.icecream.service;

import com.nashtech.exercises.icecream.dao.Faq;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface FaqService {
    List<Faq> findAll();
    Faq save(Faq faq);
}
