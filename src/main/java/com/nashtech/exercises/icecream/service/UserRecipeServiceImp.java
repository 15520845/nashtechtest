package com.nashtech.exercises.icecream.service;

import com.nashtech.exercises.icecream.dao.UserRecipe;
import com.nashtech.exercises.icecream.reponsitory.UserRecipeRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserRecipeServiceImp  implements  UserRecipeService{

    private UserRecipeRepository userRecipeRepository;

    public UserRecipeServiceImp(UserRecipeRepository userRecipeRepository) {
        this.userRecipeRepository = userRecipeRepository;
    }

    @Override
    public List<String> getNewestUserRecipe() {
        List<String> newestUserRecipeToString = new ArrayList<>();
        List<UserRecipe> userRecipes  =   userRecipeRepository.getNewestUserRecipe();
        for (UserRecipe userRecipe: userRecipes){
            newestUserRecipeToString.add(userRecipe.getName()+" of "+userRecipe.getCustomer().getFullName());
        }
        return newestUserRecipeToString;
    }
}
