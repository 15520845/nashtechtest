package com.nashtech.exercises.icecream.service;

import com.nashtech.exercises.icecream.dao.UserRecipe;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface UserRecipeService {
    List<String> getNewestUserRecipe();
}
