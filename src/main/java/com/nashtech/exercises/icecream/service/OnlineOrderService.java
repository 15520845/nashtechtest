package com.nashtech.exercises.icecream.service;

import com.nashtech.exercises.icecream.dao.OnlineOrder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface OnlineOrderService {
    List<OnlineOrder> findAll();
    OnlineOrder save(OnlineOrder onlineOrder);
}
