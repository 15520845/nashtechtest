package com.nashtech.exercises.icecream.service;

import com.nashtech.exercises.icecream.dao.Faq;
import com.nashtech.exercises.icecream.reponsitory.FaqRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FaqServiceImp implements FaqService{

    private FaqRepository faqRepository;

    public FaqServiceImp(FaqRepository faqRepository) {
        this.faqRepository = faqRepository;
    }

    @Override
    public List<Faq> findAll() {
        return faqRepository.findAll();
    }

    @Override
    public Faq save(Faq faq) {
        return faqRepository.save(faq);
    }
}
