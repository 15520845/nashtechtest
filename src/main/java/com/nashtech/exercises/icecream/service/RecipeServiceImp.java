package com.nashtech.exercises.icecream.service;

import com.nashtech.exercises.icecream.dao.Recipe;
import com.nashtech.exercises.icecream.reponsitory.RecipeRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class RecipeServiceImp implements RecipeService{

    private RecipeRepository recipeRepository;

    public RecipeServiceImp(RecipeRepository recipeRepository) {
        this.recipeRepository = recipeRepository;
    }

    @Override
    public List<Recipe> findAll() {
        return recipeRepository.findAll();
    }

    @Override
    public Recipe findById(int recipeId) {
        Optional<Recipe> recipe=recipeRepository.findById(recipeId);
        return recipe.isPresent() ? recipe.get() : null;
    }

    @Override
    public List<Recipe> getTopMostViewRecipe() {
        return recipeRepository.getTopMostViewRecipe();
    }
}
