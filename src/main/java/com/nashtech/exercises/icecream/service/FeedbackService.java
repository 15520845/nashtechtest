package com.nashtech.exercises.icecream.service;

import com.nashtech.exercises.icecream.dao.Faq;
import com.nashtech.exercises.icecream.dao.Feedback;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface FeedbackService {
    List<Feedback> findAll();
    Feedback save(Feedback feedback);
}
