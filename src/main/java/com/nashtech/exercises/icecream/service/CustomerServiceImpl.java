package com.nashtech.exercises.icecream.service;

import com.nashtech.exercises.icecream.dao.Customer;
import com.nashtech.exercises.icecream.dao.CustomerDto;
import com.nashtech.exercises.icecream.reponsitory.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;

@Service
public class CustomerServiceImpl implements CustomerService {

    private CustomerRepository customerRepository;

    public CustomerServiceImpl(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Override
    public List<Customer> findAll() {
       return customerRepository.findAll();
    }

    @Override
    public Customer save(Customer customer) {
        return customerRepository.save(customer);
    }

//    @Override
////    public Customer update(CustomerDto customerDto) {
////        Customer customer = customerRepository.findById(customerDto.getId()).get();
////        ifDefined(customerDto.getUsername(), customer.getUsername());
////        return null;
////    }
////
////    private void ifDefined(Optional<?> t, Consumer<?> consumer){
////        if (t != null && t.isPresent()){
////            consumer.accept(t.get());
////        }
////    }
}






