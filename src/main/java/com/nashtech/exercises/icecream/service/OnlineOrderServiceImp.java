package com.nashtech.exercises.icecream.service;

import com.nashtech.exercises.icecream.dao.OnlineOrder;
import com.nashtech.exercises.icecream.reponsitory.OnlineOrderRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OnlineOrderServiceImp implements OnlineOrderService {

    private OnlineOrderRepository onlineOrderRepository;

    public OnlineOrderServiceImp(OnlineOrderRepository onlineOrderRepository) {
        this.onlineOrderRepository = onlineOrderRepository;
    }

    @Override
    public List<OnlineOrder> findAll() {
        return onlineOrderRepository.findAll();
    }

    @Override
    public OnlineOrder save(OnlineOrder onlineOrder) {
        return onlineOrderRepository.save(onlineOrder);
    }
}
