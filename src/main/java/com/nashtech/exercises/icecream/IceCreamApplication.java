package com.nashtech.exercises.icecream;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
public class IceCreamApplication {
    public static void main(String[] args) throws Exception {
        SpringApplication.run(IceCreamApplication.class, args);
    }

}
